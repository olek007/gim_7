﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class play : MonoBehaviour {

    public ParticleSystem p1;
    public ParticleSystem p2;
    public bool isStarted = false;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Z))
        {
            if (!isStarted)
            {
                p1.Play();
                p2.Play();
                isStarted = true;
            }else
            {
                p1.Stop();
                p2.Stop();
                isStarted = false;
            }
        }
	}
}
